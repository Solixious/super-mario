package game.entity.creature;

import java.awt.Graphics;

import game.GameConstants;
import game.Handler;
import game.gfx.Assets;

public class Player extends Creature {

	private int direction, state, pose, walkCtr;

	public Player(Handler handler, float x, float y, int width, int height) {
		super(handler, x, y, width, height);
		direction = 0;
		state = 2;
		pose = 0;
		walkCtr = 0;
		setSpeed(4);
	}

	@Override
	public void tick() {
		if (handler.getKeyManager().right || handler.getKeyManager().left) {
			if (handler.getKeyManager().right) {
				setxMove(speed);
				direction = 0;
			}
			if (handler.getKeyManager().left) {
				setxMove(-speed);
				direction = 1;
			}
			walkCtr = (walkCtr + 1) % 60;
			if (walkCtr % 3 == 0) {
				pose = (pose + 1) % 4;
			}
		} else {
			setxMove(0);
			pose = 0;
		}

		if (!collisionWithTile((int) (x / GameConstants.TILE_WIDTH),
				(int) ((y + height + 16) / GameConstants.TILE_HEIGHT))
				&& !collisionWithTile((int) ((x + width) / GameConstants.TILE_WIDTH),
						(int) ((y + height + 10) / GameConstants.TILE_HEIGHT))) {
			pose = 5;
		} else {
			if (handler.getKeyManager().jump) {
				setyMove(-10);
			}
		}
		move();
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(Assets.MARIO[pose][state][direction], (int) (getX() - handler.getGameCamera().getxOffset()),
				(int) (getY() - handler.getGameCamera().getyOffset()), null);
	}

}
