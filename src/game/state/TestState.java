package game.state;

import java.awt.Color;
import java.awt.Graphics;

import game.GameConstants;
import game.Handler;
import game.entity.creature.Player;
import game.world.World;

public class TestState extends State {

	private World world;
	private Player player;
	private Color empty;
	
	public TestState(Handler handler) {
		super(handler);
		world = new World(handler, "res/world/testworld.txt");
		handler.setWorld(world);
		empty = new Color(128,128,255);
		player = new Player(handler, 128, 512 - 64, 32, 64);
	}

	@Override
	public void tick() {
		world.tick();
		player.tick();

		handler.getGameCamera().centerOnEntity(player);
	}

	@Override
	public void render(Graphics g) {
		g.setColor(empty);
		g.fillRect(0, 0, GameConstants.WIDTH + 10, GameConstants.HEIGHT + 10);
		world.render(g);
		player.render(g);
	}

}
