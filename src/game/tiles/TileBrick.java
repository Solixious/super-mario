package game.tiles;

import game.gfx.Assets;

public class TileBrick extends Tile {

	public TileBrick(int id) {
		super(Assets.BRICK_1, id);
	}

	@Override
	public boolean isSolid() {
		return true;
	}
}
