package game.tiles;

import game.gfx.Assets;

public class TileBlock extends Tile {
	
	public TileBlock(int id) {
		super(Assets.BLOCK_1, id);
	}

	@Override
	public boolean isSolid() {
		return true;
	}
}
