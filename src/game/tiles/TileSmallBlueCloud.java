package game.tiles;

import game.gfx.Assets;

public class TileSmallBlueCloud extends Tile {
	public TileSmallBlueCloud(int id) {
		super(Assets.SMALL_CLOUD_BLUE, id);
	}
}
