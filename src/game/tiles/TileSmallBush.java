package game.tiles;

import game.gfx.Assets;

public class TileSmallBush extends Tile {
	public TileSmallBush(int id) {
		super(Assets.SMALL_BUSH, id);
	}
}
