package game.tiles;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import game.GameConstants;

public class Tile {

	public static Tile[] tiles = new Tile[GameConstants.TILE_COUNT];

	public static Tile emptyTile = new TileEmpty(0);
	public static Tile brickTile = new TileBrick(1);
	public static Tile floorTile = new TileFloor(2);
	public static Tile smallBlueCoudTile = new TileSmallBlueCloud(3);
	public static Tile bigBlueCoudTile = new TileBigBlueCloud(4);
	public static Tile smallBushTile = new TileSmallBush(5);
	public static Tile bigBushTile = new TileBigBush(6);
	public static Tile blockTile = new TileBlock(7);
	public static Tile questionBlockTile = new TileQuestionBlock(8);
	
	public static int tileWidth = GameConstants.TILE_WIDTH;
	public static int tileHeight = GameConstants.TILE_HEIGHT;

	protected BufferedImage texture;
	protected final int id;

	public Tile(BufferedImage texture, int id) {
		this.texture = texture;
		this.id = id;

		tiles[id] = this;
	}
	
	public void setTexture(BufferedImage texture) {
		this.texture = texture;
	}

	public void tick() {
		
	}

	public void render(Graphics g, int x, int y) {
		g.drawImage(texture, x, y, texture.getWidth(), texture.getHeight(), null);
	}

	public boolean isSolid() {
		return false;
	}

	public int getID() {
		return id;
	}
}
