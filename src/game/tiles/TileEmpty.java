package game.tiles;

import game.gfx.Assets;

public class TileEmpty extends Tile {

	public TileEmpty(int id) {
		super(Assets.EMPTY, id);
	}

	@Override
	public boolean isSolid() {
		return false;
	}
}
