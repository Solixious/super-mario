package game.tiles;

import game.gfx.Assets;

public class TileQuestionBlock extends Tile {

	private int ctr = 0, in = 0;
	
	public TileQuestionBlock(int id) {
		super(Assets.QUESTION_1[0], id);
	}

	@Override
	public boolean isSolid() {
		return true;
	}
	
	@Override
	public void tick() {
		ctr = (ctr + 1) % 60;
		if(ctr % 15 == 0) {
			in = (in + 1) % 3;
			setTexture(Assets.QUESTION_1[in]);
		}
	}
}
