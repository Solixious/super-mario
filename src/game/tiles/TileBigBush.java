package game.tiles;

import game.gfx.Assets;

public class TileBigBush extends Tile {
	public TileBigBush(int id) {
		super(Assets.BIG_BUSH, id);
	}
}
