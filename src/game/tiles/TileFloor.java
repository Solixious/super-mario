package game.tiles;

import game.gfx.Assets;

public class TileFloor extends Tile {
	
	public TileFloor(int id) {
		super(Assets.FLOOR_1, id);
	}

	@Override
	public boolean isSolid() {
		return true;
	}
}
