package game.tiles;

import game.gfx.Assets;

public class TileBigBlueCloud extends Tile {
	public TileBigBlueCloud(int id) {
		super(Assets.BIG_CLOUD_BLUE, id);
	}
}
