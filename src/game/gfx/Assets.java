package game.gfx;

import java.awt.Color;
import java.awt.image.BufferedImage;

import game.utils.Utils;

public class Assets {
	public static BufferedImage EMPTY;
	public static BufferedImage BRICK_1, BRICK_2, BRICK_3;
	public static BufferedImage BLOCK_1, BLOCK_2, BLOCK_3;
	public static BufferedImage FLOOR_1, FLOOR_2, FLOOR_3;
	public static BufferedImage SMALL_HILL, BIG_HILL;
	public static BufferedImage SMALL_CLOUD_BLUE, BIG_CLOUD_BLUE;
	public static BufferedImage SMALL_CLOUD_RED, BIG_CLOUD_RED;
	public static BufferedImage SMALL_BUSH, BIG_BUSH;
	public static BufferedImage MUSHROOM_RED, MUSHROOM_GREEN;
	public static BufferedImage[] STAR;
	public static BufferedImage[] QUESTION_1, QUESTION_2, QUESTION_3;
	public static BufferedImage[] COIN_1, COIN_2, COIN_3;
	public static BufferedImage[] FIRE_BALL;
	public static BufferedImage[] FLOWER_1, FLOWER_2;
	public static BufferedImage[] TUNNEL_1, TUNNEL_2;
	public static BufferedImage[][][] MARIO;
	
	public static void init() {
		BufferedImage img1 = ImageLoader.loadImage("/texture/ss1.png");
		BufferedImage img2 = ImageLoader.loadImage("/texture/ss2.png");
		SpriteSheet ss1 = new SpriteSheet(img1, new Color(192, 192, 192));
		SpriteSheet ss2 = new SpriteSheet(img2, Color.GREEN);

		EMPTY = ss1.crop(0, 0, 32, 32);
		
		BRICK_1 = ss1.crop(938, 90, 32, 32);
		BRICK_2 = ss1.crop(938, 127, 32, 32);
		BRICK_3 = ss1.crop(938, 166, 32, 32);

		BLOCK_1 = ss1.crop(770, 250, 32, 32);
		BLOCK_2 = ss1.crop(808, 250, 32, 32);
		BLOCK_3 = ss1.crop(846, 250, 32, 32);

		FLOOR_1 = ss1.crop(770, 289, 32, 32);
		FLOOR_2 = ss1.crop(808, 289, 32, 32);
		FLOOR_3 = ss1.crop(846, 289, 32, 32);

		QUESTION_1 = new BufferedImage[4];
		QUESTION_1[0] = ss1.crop(770, 90, 32, 32);
		QUESTION_1[1] = ss1.crop(812, 90, 32, 32);
		QUESTION_1[2] = ss1.crop(854, 90, 32, 32);
		QUESTION_1[3] = ss1.crop(896, 90, 32, 32);

		QUESTION_2 = new BufferedImage[4];
		QUESTION_2[0] = ss1.crop(770, 128, 32, 32);
		QUESTION_2[1] = ss1.crop(812, 128, 32, 32);
		QUESTION_2[2] = ss1.crop(854, 128, 32, 32);
		QUESTION_2[3] = ss1.crop(896, 128, 32, 32);

		QUESTION_3 = new BufferedImage[4];
		QUESTION_3[0] = ss1.crop(770, 166, 32, 32);
		QUESTION_3[1] = ss1.crop(812, 166, 32, 32);
		QUESTION_3[2] = ss1.crop(854, 166, 32, 32);
		QUESTION_3[3] = ss1.crop(896, 166, 32, 32);

		FIRE_BALL = new BufferedImage[3];
		FIRE_BALL[0] = ss1.crop(888, 50, 16, 16);
		FIRE_BALL[1] = ss1.crop(910, 44, 24, 28);
		FIRE_BALL[2] = ss1.crop(940, 42, 32, 32);

		COIN_1 = new BufferedImage[4];
		COIN_1[0] = ss1.crop(176, 120, 20, 28);
		COIN_1[1] = ss1.crop(176, 158, 20, 28);
		COIN_1[2] = ss1.crop(176, 196, 20, 28);
		COIN_1[3] = ss1.crop(176, 234, 20, 28);

		COIN_2 = new BufferedImage[4];
		COIN_2[0] = ss1.crop(200, 120, 20, 28);
		COIN_2[1] = ss1.crop(200, 158, 20, 28);
		COIN_2[2] = ss1.crop(200, 196, 20, 28);
		COIN_2[3] = ss1.crop(200, 234, 20, 28);

		COIN_3 = new BufferedImage[4];
		COIN_3[0] = ss1.crop(224, 120, 20, 28);
		COIN_3[1] = ss1.crop(224, 158, 20, 28);
		COIN_3[2] = ss1.crop(224, 196, 20, 28);
		COIN_3[3] = ss1.crop(224, 234, 20, 28);

		SMALL_HILL = ss1.crop(522, 12, 96, 64);
		BIG_HILL = ss1.crop(629, 6, 160, 64);

		SMALL_CLOUD_BLUE = ss1.crop(568, 100, 64, 48);
		BIG_CLOUD_BLUE = ss1.crop(562, 156, 96, 48);
		SMALL_CLOUD_RED = ss1.crop(664, 156, 64, 48);
		BIG_CLOUD_RED = ss1.crop(638, 100, 96, 48);

		SMALL_BUSH = ss1.crop(550, 218, 64, 32);
		BIG_BUSH = ss1.crop(622, 218, 128, 32);

		MUSHROOM_RED = ss1.crop(288, 84, 32, 32);
		MUSHROOM_GREEN = ss1.crop(326, 84, 32, 32);
		
		FLOWER_1 = new BufferedImage[4];
		FLOWER_1[0] = ss1.crop(258, 130, 32, 32);
		FLOWER_1[1] = ss1.crop(296, 130, 32, 32);
		FLOWER_1[2] = ss1.crop(334, 130, 32, 32);
		FLOWER_1[3] = ss1.crop(372, 130, 32, 32);
		
		FLOWER_2 = new BufferedImage[4];
		FLOWER_2[0] = ss1.crop(258, 170, 32, 32);
		FLOWER_2[1] = ss1.crop(296, 170, 32, 32);
		FLOWER_2[2] = ss1.crop(334, 170, 32, 32);
		FLOWER_2[3] = ss1.crop(372, 170, 32, 32);
		
		STAR = new BufferedImage[5];
		STAR[0] = ss1.crop(256, 210, 28, 32);
		STAR[1] = ss1.crop(294, 210, 28, 32);
		STAR[2] = ss1.crop(334, 210, 28, 32);
		STAR[3] = ss1.crop(372, 210, 28, 32);
		STAR[4] = ss1.crop(316, 250, 28, 32);

		TUNNEL_1 = new BufferedImage[4];
		TUNNEL_1[0] = ss1.crop(152, 416, 32, 32);
		TUNNEL_1[1] = ss1.crop(184, 416, 32, 32);
		TUNNEL_1[2] = ss1.crop(152, 448, 32, 32);
		TUNNEL_1[3] = ss1.crop(184, 448, 32, 32);
		

		TUNNEL_2 = new BufferedImage[4];
		TUNNEL_2[0] = ss1.crop(304, 416, 32, 32);
		TUNNEL_2[1] = ss1.crop(336, 416, 32, 32);
		TUNNEL_2[2] = ss1.crop(304, 448, 32, 32);
		TUNNEL_2[3] = ss1.crop(336, 448, 32, 32);
		
		MARIO = new BufferedImage[6][3][2];
		MARIO[0][0][0] = ss2.crop(90, 6, 24, 32);
		MARIO[1][0][0] = ss2.crop(120, 6, 26, 32);
		MARIO[2][0][0] = ss2.crop(151, 6, 30, 32);
		MARIO[3][0][0] = ss2.crop(192, 4, 22, 32);
		MARIO[4][0][0] = ss2.crop(220, 6, 26, 32);
		MARIO[5][0][0] = ss2.crop(250, 4, 32, 32);

		MARIO[0][1][0] = ss2.crop(90, 82, 32, 64);
		MARIO[1][1][0] = ss2.crop(128, 82, 32, 64);
		MARIO[2][1][0] = ss2.crop(166, 85, 32, 64);
		MARIO[3][1][0] = ss2.crop(204, 82, 28, 64);
		MARIO[4][1][0] = ss2.crop(238, 82, 34, 64);
		MARIO[5][1][0] = ss2.crop(278, 82, 32, 64);

		MARIO[0][2][0] = ss2.crop(92, 220, 32, 64);
		MARIO[1][2][0] = ss2.crop(130, 220, 32, 64);
		MARIO[2][2][0] = ss2.crop(168, 223, 32, 64);
		MARIO[3][2][0] = ss2.crop(206, 220, 28, 64);
		MARIO[4][2][0] = ss2.crop(240, 220, 34, 64);
		MARIO[5][2][0] = ss2.crop(280, 220, 32, 64);
		
		for(int i = 0; i < MARIO.length; i++) {
			for(int j = 0; j < MARIO[i].length; j++) {
				MARIO[i][j][1] = Utils.flipImageHorizontally(MARIO[i][j][0]);
			}
		}
	}
}
